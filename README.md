
## Entry for: January Nexys4 Board Giveaway Challenge

### Competition description
This months challenge is a size optimization challenge. Your task is to reproduce the image shown below on the C64. Entries must be in PRG format and load into VICE 3.4+ using inject to RAM only! The smallest prg size in bytes wins. 

The competition will run until January 30th and the winner will be announced on stream. Please send your entries to me via private message only and do not share your code with anyone until after the competition closes. In the event of a tie winner will be chosen randomly on stream using a method of my choice. Winner will receive a Nexys4 board preconfigured with the Mega65 core.

Just a further point, final PRG compression is NOT allowed as it goes against the spirit of the challenge.

![contest image](images/challenge.png)