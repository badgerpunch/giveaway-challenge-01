!cpu 6510

;DEBUG = 1

nr_Of_Lines   =  $16      ; $16 is #25 on startup
chars2print   = $17
direc         = $18

screen_lo     = $52      ; $D1 is #00 on startup
screen_hi     = $53      ; $D2 is #04 on startup

number1       = $2b       ; is #01 at start
number2       = $76       ; is #02 at start

CHAR          = $80       ; PETScii code for inverted @


!ifndef DEBUG {
address_code              = $1fa
}

!ifdef DEBUG {
address_basic             = $0801
address_code              = $080E

*=address_basic
  
!byte $0d,$08,$dc,$07,$9e,$20,$32,$30   ; BASIC to load $1000
!byte $36,$32,$00,$00,$00               ; inserts BASIC line: 2012 SYS 4096

* = address_code                        ; start address for 6502 code 
}
!ifndef DEBUG {

* = $1f8  ;Override return from load vector on stack         
    !byte <Entry-1, >Entry-1
}

* = address_code                        ; start address for 6502 code 
Entry:  
  jsr $e544           ; Clear screen

outer_loop:
  lax nr_Of_Lines     ; Load into A and X
  ldy #40             ; Vertical line add/sub
  bit number1
  beq +
  ldy #1              ; Horisontal line add/sub
  clc
  adc #$10            ; Line length follows nr_Of_Lines on horizontal
                      ; But vertical lines are 16 chars longer.
+ 
  sta chars2print     ; How many chars for this line
  sty direc           ; the direction horiz/vert

inner_loop:
  txa                 ; X has nr_of_lines left
  lsr                 ; Divide by 4 because we are alternating
  lsr                 ; between add/sub every 2 lines.
  lda screen_lo
  bcc add_it          ; if <2 then we add
  ; Lets subtract
  sbc direc
  bcs no_hibyte_add
  dec screen_hi
  bpl no_hibyte_add
add_it:
  adc direc
  bcc no_hibyte_add
  inc screen_hi
no_hibyte_add:
  sta screen_lo         ; Store new low-byte adr

  lda #CHAR             ; Char to print...
  ldy #$FE              ; Added to index, because line start in mem is $03FE
  sta (screen_lo),y

  dec chars2print
  bne inner_loop

  dec nr_Of_Lines
  bne outer_loop

  bpl *
